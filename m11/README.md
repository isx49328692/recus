# Servidor LDAPS

## Arrancar juan22/tls22:ldaps

    sudo docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -p 636:636 --network 2hisx -d juan22/tls22:ldaps

## Ingresar interactivamente

    sudo docker exec -it ldap.edt.org /bin/bash

## Comprovacion dentro del container

    ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'   :389
    ldapsearch -x -Z -LLL -h ldap.edt.org
    ldapsearch -x -ZZ -LLL -h ldap.edt.org

    ldapsearch -x -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -LLL -H ldaps://localhost -b 'dc=edt,dc=org'
    ldapsearch -x -LLL -H ldaps://127.0.0.1 -b 'dc=edt,dc=org'

    ldapsearch -x -ZZ -LLL -H ldap://ldap.edt.org -b 'dc=edt,dc=org'    :389
    ldapsearch -x -ZZ -LLL -H ldap://localhost  :389
    ldapsearch -x -Z -LLL -H ldap://ldap.edt.org    :389

    ldapsearch -x -LLL -H ldaps://ldap.edt.org  :363
    ldapsearch -x -Z -LLL -H ldaps://ldap.edt.org   :636
    ldapsearch -x -ZZ -LLL -H ldaps://ldap.edt.org --> ERROR: ldaps:// ya es seguro -ZZ obliga a que lo sea (=) :636

## Comprovacion desde host

    ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -LLL -h ldapserver.exemple.org -b 'dc=edt,dc=org'
    ldapsearch -x -LLL -h 127.0.0.1 -b 'dc=edt,dc=org'
    ldapsearch -x -Z -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -ZZ -LLL -h ldap.edt.org -b 'dc=edt,dc=org'

    ldapsearch -x -ZZ -LLL -H ldap://ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -Z -LLL -H ldap://ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -Z -LLL -H ldap://ldapserver.exemple.org -b 'dc=edt,dc=org'

    ldapsearch -x -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -Z -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
    ldapsearch -x -ZZ -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org' --> ERROR TLS already started

* Se puede añadir "-d1" para más información
