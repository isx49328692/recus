# LDAPS

Partiendo de la imagen juan22/ldap22:groups, creo la nueva imagen juan22/tls22:ldaps, un servidor ldap seguro que, ha d’utilitzar els seus propis certificats digitals i ha de permetre tant connexions ldaps com connexions ldap amb starttls.

Utilitzarem sempre una autoritat de certificació CA anomenada Veritat Absoluta, que
és qui emetrà tots els certificats per a l’organització edt.org. Aquí caldrà un certificat de servidor per al CN ldap.edt.org però també ha de ser vàlid per a dos alies, l’adreça IP del servidor i el loopback (useu Subject Alternate Name).

Un client ldap ha de poser-se conectar al servidor ldap usant ldaps connectant al
port privilegiat, però també usant ldap (port insegur) i usant startls per generar una
connexió segura.

Assegureu-vos de practicar amb telnet, curl, i ldapsearch (amb zetes!) i amb openssl
s_client fins a tenir-ho clar.

# instalar

instalamos openssl en el servidor ldap, lo añadimos a su Dockerfile y ademas en la linea 'EXPOSE 389' añadimos el puerto 636'

Del directorio editamos tambien startup.sh y en la linea "/usr/sbin/slapd -d3 -h"
añadimos ' "ldap:/// ldaps:/// ldapi:///" '

# Configuracion

Modificamos ldap.conf:


    # TLS certificates (needed for GnuTLS)
    #TLS_CACERT /etc/ssl/certs/ca-certificates.crt
    TLS_CACERT  /etc/ldap/certs/ca.crt
    TLS_KEY		/etc/ldap/certs/serverkey.ldap.pem

Modificamos slapd.conf para configurar la capa de segurirdad TLS/SSL:

    TLSCACertificateFile    /etc/ldap/certs/ca.crt
    TLSCertificateFile      /etc/ldap/certs/servercert.ldap.crt
    TLSCertificateKeyFile   /etc/ldap/certs/serverkey.ldap.pem
    TLSVerifyClient         never


    TLSCACertificateFile    /etc/ldap/certs/ca.crt:

        Esta directiva especifica la ubicación del archivo de certificado de la Autoridad de Certificación (CA, por sus siglas en inglés) en el servidor. El archivo ca.crt contiene el certificado de la CA utilizada para firmar los certificados de los servidores y clientes LDAP.

    TLSCertificateFile  /etc/ldap/certs/servercert.ldap.crt:

        Esta directiva indica la ubicación del archivo de certificado del servidor LDAP. El archivo servercert.ldap.crt contiene el certificado del servidor LDAP, que se utiliza para autenticar y establecer la comunicación segura con los clientes LDAP.

    TLSCertificateKeyFile   /etc/ldap/certs/serverkey.ldap.pem:
        Esta directiva especifica la ubicación del archivo de clave privada correspondiente al certificado del servidor LDAP. El archivo serverkey.ldap.pem contiene la clave privada que corresponde al certificado del servidor, necesaria para establecer la comunicación segura.

    TLSVerifyClient never:
        Esta directiva indica la política de verificación del cliente LDAP. En este caso, se establece en "never", lo que significa que no se requerirá la verificación del cliente LDAP. Es decir, cualquier cliente que se conecte al servidor LDAP será aceptado sin la necesidad de presentar un certificado de cliente.

