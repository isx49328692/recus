# Practica 15 SSH + AWS

## Parte 1: Desplegar en el cloud

   - Desplegar ldap (juan22/ldap:group) y phpldapadmin (edtasaixm06/ldap22:phpldapadmin) mediante docker-compose up -d
   - Contactar con ldap desde localhost: ldapsearch -x -LLL -h 3.19.82.188 -b 'dc=edt,dc=org'
   - Verificar funcionamiento phpldapadmin: desde navegador 3.19.82.188/phpldapadmin
   - Security-wizard --> ssh-ldap-php (636, 22, 80, 389, 443, 2022, 8080)
   - Desplegar host PAM (juan22/pam22:ldap / juan22/pam22:sshfs)
   - Configurar interactivamente PAM --> IP cloud --> Auntenticar ldap

    - /etc/host --> 3.19.82.188 ldap.edt.org
    - inicio de sesion local: su - unix01
    - inicio de sesion ldap: su - anna
   
   - Servicio phpldapadmin: 3.19.82.188/phpldapadmin

## Parte 2: Personalizar Cloud

   - Generar AMI (2hisxAMI)
   - IP elastica 3.19.82.188
   - modificar interactivamente PAM (/etc/hosts) --> 3.19.82.188 ldap.edt.org

## Parte 3: Servei ssh al cloud

   - Desplegar ldap, ssh (juan22/ssh22:base) y phpldapadmin: docker-compose up -d
   - En el host PAM instalar openssh-client,sshfs (añadir al Dockerfile)
   - Inicio de sesion local y ldap
   - Inicio de sesion ssh:

    - ssh unix01@3.19.82.188 -p 2022
    - ssh anna@3.19.82.188 -p 2022

   - Inicio de sesion unix01 y anna mediante pubkey

    - En el container ssh.edt.org (/etc/ssh/sshd_config):
        - PasswordAuthentication no --> no necesario
        - PubKeyAuthentication yes

    - En el host PAM:
        - ssh-keygen -t rsa
        - ssh-copy-id -p 2022 user@3.19.82.188

    - Verificamos:
        - ssh -p 2022 -i ~/.ssh/id_rsa unix01@3.19.82.188
        - ssh -p 2022 unix01@3.19.82.188
        - ssh -p 2022 anna@3.19.82.18

## Parte 4: Montaje de homes via ssh

   - Desplegar ssh - ldap - phpldapadmin en aws: docker-compose up -d
   - Desplegar host PAM local --> editar /etc/hosts --> 3.19.82.188 ssh.edt.org
   - Configruar /etc/security/pam-mount.conf.xml
   - Verificar:
   
    - locals unix tmpfs - 100M
    - locals unix --> /home/user/tmpfs
    - ldap --> /home/user/(user, tmpfs)

## Desplagamiento:

   - En AWS:
      
      - sudo docker-compose up -d
   
   - En local:
      
      - docker run --rm --name sshfs.edt.org --hostname sshfs.edt.org --net 2hisx --privileged --cap-add SYS_ADMIN --device /dev/fuse  --security-opt apparmor:unconfined -it juan22/pam22:sshfs