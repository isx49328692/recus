#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done


cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
#cp /opt/docker/common-account /etc/pam.d/common-account
#cp /opt/docker/common-auth /etc/pam.d/common-auth
#cp /opt/docker/common-password /etc/pam.d/common-password
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
rm /etc/hosts
cp /opt/docker/hosts /etc/hosts

mkdir /root/.ssh
chmod 700 /root/.ssh
#cp /opt/docker/known_hosts /root/.ssh/known_hosts

#ssh-keygen -t rsa
#ssh-copy-id -p 2022 unix01@3.19.82.188
#ssh-copy-id -p 2022 anna@3.19.82.188

ssh-keyscan -p 2022 3.19.82.188 >> /root/.ssh/known_hosts

/usr/sbin/nscd
/usr/sbin/nslcd
/bin/bash



