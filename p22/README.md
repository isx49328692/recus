
version 1:

    docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d juan22/ldap22:samba

    docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -d juan22/samba22:home_server

    docker logs samba.edt.org

    docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it juan22/pam22:samba

smbclient -U user%user \\server\user

version 2:

    docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d juan22/ldap22:samba

    docker run --rm --name samba.edt.org -h samba.edt.prg --net 2hisx --privileged -d juan22/samba22:smb_home_server

    docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it juan22/pam22:samba